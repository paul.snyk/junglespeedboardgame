﻿using System.Collections;
using UnityEngine;
using System.Collections.Generic;

public abstract class State
{
    protected ControllerCard ControllerCard;

    public State(ControllerCard controllerCard)
    {
        ControllerCard = controllerCard;
    }
    public virtual IEnumerator PiochePlayer1()
    {
        yield break;
    }
    public virtual IEnumerator PiochePlayer2()
    {
        yield break;
    }
}
