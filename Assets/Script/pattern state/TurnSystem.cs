﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnSystem : State
{
    public TurnSystem(ControllerCard controllerCard) : base(controllerCard)
    {
        
    }
    public override IEnumerator PiochePlayer1()
    {
        ControllerCard.validationP2.SetActive(false);
        ControllerCard.validationP1.SetActive(true);
        ControllerCard.piocheP2.SetActive(false);
        ControllerCard.piocheP1.SetActive(true);
        ControllerCard.piocheOk = false;
        if (ControllerCard.packPlayer1.Count == 0)
        {
            if (ControllerCard.cardPlayPlayer1.Count == 0)
            {
                ControllerCard.infosPartie.text = "C'est le tour du Joueur 1, vous n'avez plus aucune carte. Félicitation vous remportez la victoire";
            }
            else if (ControllerCard.cardPlayPlayer1.Count >= 1)
            {
                ControllerCard.infosPartie.text = "C'est le tour du Joueur 1, vous ne pouvez plus pioché. Validé pour continue";
            }
        }
        else if (ControllerCard.packPlayer1.Count >= 1)
        {
            ControllerCard.infosPartie.text = "C'est le tour du Joueur 1, vous pouvez pioché";
        }
        yield break;
        
    }
    public override IEnumerator PiochePlayer2()
    {
        ControllerCard.validationP2.SetActive(true);
        ControllerCard.validationP1.SetActive(false);
        ControllerCard.piocheP2.SetActive(true);
        ControllerCard.piocheP1.SetActive(false);
        ControllerCard.piocheOk = false;
        if (ControllerCard.packPlayer2.Count == 0)
        {
            if (ControllerCard.cardPlayPlayer2.Count == 0)
            {
                ControllerCard.infosPartie.text = "C'est le tour du Joueur 2, vous n'avez plus aucune carte. Félicitation vous remportez la victoire";
            }
            else if (ControllerCard.cardPlayPlayer2.Count >= 1)
            {
                ControllerCard.infosPartie.text = "C'est le tour du Joueur 2, vous ne pouvez plus pioché. Validé pour continue";
            }
        }
        else if (ControllerCard.packPlayer2.Count >= 1)
        {
            ControllerCard.infosPartie.text = "C'est le tour du Joueur 2, vous pouvez pioché";
        }
        yield break;
    }
}
