﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardDisplay : MonoBehaviour
{
    public Card card;

    public Image imageArtwork;

    public int valueCard;
    void Start()
    {
        if (card == null)
        {
            imageArtwork.gameObject.SetActive(false);
        }
    }

    public void Affichage()
    {
        valueCard = card.descriptionValue;
        imageArtwork.gameObject.SetActive(true);
        imageArtwork.sprite = card.imageArtwork;
        Debug.Log("value de la card : " + valueCard);
    }
}
