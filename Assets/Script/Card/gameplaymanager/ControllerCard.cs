﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ControllerCard : StateMachine
{
    public List<Card> starterDeck;

    public List<Card> packPlayer1 = new List<Card>();

    public List<Card> packPlayer2 = new List<Card>();

    public List<Card> cardPlayPlayer1 = new List<Card>();

    public List<Card> cardPlayPlayer2 = new List<Card>();

    public TMP_Text numbCardPlayer1;
    public TMP_Text numbCardPlayer2;
    public TMP_Text infosPartie;
    public TMP_Text timerTotemAffichage;

    public bool piocheOk;
    public bool blocageState;
    public bool remplissagedeck;
    public bool totemRecup;
    public bool player1Win;
    public bool player2Win;
    public bool player1Lose;
    public bool player2Lose;
    
    private float timerTotem;
    
    public GameObject cardDisplayPlayer1;
    public GameObject cardDisplayPlayer2;

    public GameObject validationP1;
    public GameObject validationP2;
    public GameObject piocheP1;
    public GameObject piocheP2;
    
    // Start is called before the first frame update
    void Start()
    {
        // on cherche des gameobject dans la scene
        validationP1 = GameObject.Find("ButtonValidationP1");
        piocheP1 = GameObject.Find("ButtonPiocheP1");
        validationP2 = GameObject.Find("ButtonValidationP2");
        piocheP2 = GameObject.Find("ButtonPiocheP2");
    }

    // Update is called once per frame
    void Update()
    {
        //affichage carte dans chaque deck des 2 joueurs
        numbCardPlayer1.text = "Nombre de cartes dans le deck du Joueur 1 : " + packPlayer1.Count;
        numbCardPlayer2.text = "Nombre de cartes dans le deck du Joueur 2 : " + packPlayer2.Count;

        //gestion timer après recup du totem
        if (totemRecup)
        {
            timerTotem -= Time.deltaTime;
            float timerArrondi =
                Mathf.RoundToInt(timerTotem); // on arrondi le float pour le rendre un peu plus beau a l'ecran
            timerTotemAffichage.text = "La partie reprend dans : " + timerArrondi; //affichage text pour le timer
         
        
            
            if (timerTotem <= 0)
            {
                player1Win = false;
                player2Win = false;
                player1Lose = false;
                player2Lose = false;
                totemRecup = false;   // on repasse le bool a false si timer arrive a zero
            }
        }
        else
        {
            timerTotemAffichage.text = ""; // on affiche rien sur le texte si bool false
        }

        // repartition du deck de départ entre les 2 deux des joueurs
        if (starterDeck.Count > 0)
        {
            int randomCard = Random.Range(0, starterDeck.Count); // le mélange des cartes est fait içi
            if (remplissagedeck)
            {
                packPlayer1.Add(starterDeck[randomCard]);
                starterDeck.Remove(starterDeck[randomCard]);
                remplissagedeck = false;
            }

            if (!remplissagedeck)
            {
                packPlayer2.Add(starterDeck[randomCard]);
                starterDeck.Remove(starterDeck[randomCard]);
                remplissagedeck = true;
            }
        }

        // lorsqu'il n'y a plus rien dans le starter deck alors on lance le systeme de state 1 fois et empeche de le relancer avec un bool car le lancement du state est dans l'update
        if (starterDeck.Count == 0)
        {
            if (!blocageState)
            {
                LancementState();
                blocageState = true;
            }
        }

        // systeme recupération totem player 1 et check condition de victoire et activation du timer
        if (Input.GetButtonDown("TottemPlayer1"))
        {
            Debug.Log("totem 1");
            if (!totemRecup)
            {
                timerTotem = 5;
                totemRecup = true;
            }

            int cardValuePlayer1 = cardDisplayPlayer1.GetComponent<CardDisplay>().valueCard;
            int cardValuePlayer2 = cardDisplayPlayer2.GetComponent<CardDisplay>().valueCard;
            if (cardValuePlayer1 == cardValuePlayer2)
            {
                player1Win = true;
            }
            else
            {
                player1Lose = true;
            }
          

        }

        // systeme recupération totem player 2 et check condition de victoire et activation du timer
        if (Input.GetButtonDown("TottemPlayer2"))
        {
            Debug.Log("totem 2");
            if (!totemRecup)
            {
                timerTotem = 5;
                totemRecup = true;
            }

            int cardValuePlayer1 = cardDisplayPlayer1.GetComponent<CardDisplay>().valueCard;
            int cardValuePlayer2 = cardDisplayPlayer2.GetComponent<CardDisplay>().valueCard;
            if (cardValuePlayer1 == cardValuePlayer2)
            {
                Debug.Log("j2 gagne");
                player2Win = true;
            }
            else
            {
                player2Lose = true;
            }
            
        }
        
      
        if (player1Win)
        {
            for (int i = 0; i <= cardPlayPlayer1.Count; i++) 
            {
                packPlayer2.Add(cardPlayPlayer2[i]);
                packPlayer2.Add(cardPlayPlayer1[i]);
                cardPlayPlayer2.Remove(cardPlayPlayer2[i]);
                cardPlayPlayer1.Remove(cardPlayPlayer1[i]);
            }
        }
            
        if (player2Win)
        {
            for (int i = 0; i <= cardPlayPlayer2.Count; i++)
            {
                packPlayer1.Add(cardPlayPlayer2[i]);
                packPlayer1.Add(cardPlayPlayer1[i]);
                cardPlayPlayer2.Remove(cardPlayPlayer2[i]);
                cardPlayPlayer1.Remove(cardPlayPlayer1[i]);
            }
        }
        
        if (player1Lose)
        {
            for (int i = 0; i <= cardPlayPlayer1.Count; i++) 
            {
                packPlayer1.Add(cardPlayPlayer2[i]);
                packPlayer1.Add(cardPlayPlayer1[i]);
                cardPlayPlayer2.Remove(cardPlayPlayer2[i]);
                cardPlayPlayer1.Remove(cardPlayPlayer1[i]);
            }
        }
        if (player2Lose)
        {
            for (int i = 0; i <= cardPlayPlayer1.Count; i++) 
            {
                packPlayer2.Add(cardPlayPlayer2[i]);
                packPlayer2.Add(cardPlayPlayer1[i]);
                cardPlayPlayer2.Remove(cardPlayPlayer2[i]);
                cardPlayPlayer1.Remove(cardPlayPlayer1[i]);
            }
        }
        
    }

    public void ButtonValidationPlayer1()
    {
        if (!totemRecup)
        {
            if (packPlayer1.Count == 0)
            {
                StartCoroutine(State.PiochePlayer2());
            }
            else if (piocheOk)
            {
                StartCoroutine(State.PiochePlayer2());
            }
            else
            {
                infosPartie.text = "Vous ne pouvez pas validez sans avoir joué. Merci de pioché une nouvelle carte Joueur 1 !";
            }
        }
    }
    public void ButtonValidationPlayer2()
    {
        if (!totemRecup)
        {
            if (packPlayer2.Count == 0)
            {
                StartCoroutine(State.PiochePlayer1());
            }
            else if (piocheOk)
            {
                StartCoroutine(State.PiochePlayer1());
            }
            else
            {
                infosPartie.text = "Vous ne pouvez pas validez sans avoir joué. Merci de pioché une nouvelle carte Joueur 2 !";
            }
        }
    }

    // void activé par boutton pour pioche player 2
    public void PiocheDeckPlayer2()
    {
        //on check les conditions
        if (!totemRecup)
        {
            if (!piocheOk)
            {
                cardPlayPlayer2.Add(packPlayer2[0]);
                int sizeListPlayer2 = cardPlayPlayer2.Count;
                packPlayer2.Remove(packPlayer2[0]);
                cardDisplayPlayer2.GetComponent<CardDisplay>().card = cardPlayPlayer2[sizeListPlayer2 - 1];
                cardDisplayPlayer2.GetComponent<CardDisplay>().Affichage();
                piocheOk = true;
            }
            if (piocheOk)
            {
                infosPartie.text = "Vous ne pouvez plus piochez ! Merci de validé";
            } 
        }
        
    }
    
    // void activé avec le boutton sur le jeu pour pioche player 1
    public void PiocheDeckPlayer1(int valuePLayer)
    {
        if (!totemRecup)
        {
            if (!piocheOk)
            {
                cardPlayPlayer1.Add(packPlayer1[0]);
                int sizeListPlayer1 = cardPlayPlayer1.Count;
                packPlayer1.Remove(packPlayer1[0]);
                cardDisplayPlayer1.GetComponent<CardDisplay>().card = cardPlayPlayer1[sizeListPlayer1 - 1];
                cardDisplayPlayer1.GetComponent<CardDisplay>().Affichage();
                piocheOk = true;
            }
            if (piocheOk)
            {
                infosPartie.text = "Vous ne pouvez plus piochez ! Merci de validé";
            }
        }
    }
    public void LancementState()
    {
        SetState(new TurnSystem(controllerCard: this));
    }
}
