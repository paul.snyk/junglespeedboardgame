﻿using System.Collections;// c'est une bibliothèque utilisé dans le script
using System.Collections.Generic;// c'est une bibliothèque utilisé dans le script
using UnityEngine;// c'est une bibliothèque utilisé dans le script

[CreateAssetMenu(fileName = "New Card")]// ajout d'un element de creation rapide comme les folder ou les script
public class Card : ScriptableObject// on change le monobehaviour
{
    public new string name;// nom de la card en question pas obligatoire mais c'est cool d'avoir un nom
    public Sprite imageArtwork;// stockage de l'image directement dans l'objet
    public int descriptionValue;//stockage d'une value direct dans l'objet pour comparaison rapide après
    public string colorName; // attribution value pour les différentes couleur pour les carte bonus(jaune orange violet vert)
    public string specialCard;
}
